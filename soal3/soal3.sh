#!/bin/bash 

# safety jika modul 2 udah dicreate
if [ -d ~/modul2 ]
then
    # echo `mkdir ~/modul2`
    sleep 3s
    echo `mkdir ~/modul2/darat`
    sleep 3s
    echo `mkdir ~/modul2/air`
    # unzip
    echo `unzip ~/Downloads/animal.zip -d ~/modul2`
    echo `mv ~/modul2/animal/**air**.jpg ~/modul2/air`
    echo `mv ~/modul2/animal/**darat**.jpg ~/modul2/darat`
    echo `rm -r ~/modul2/animal`
    echo `rm -r ~/modul2/darat/**bird**.jpg`

    # links, owner name, owner group, file size, time of last modification, and the file or directory 
    # menghitung jumlah folder di ~/modul2/air
    total=`ls -l ~/modul2/air | wc -l`

    # membuat file list.txt di luar folder air
    # karena jumlah file akan berubah jika lngsung dimasukkan
    echo `touch ~/modul2/list.txt`
    for((i=2; i<=$total; i=i+1))
    do
        strTitle="ls -l ~/modul2/air | awk '{print \$9}' | awk 'NR == $i {print}'"
        strUser="ls -l ~/modul2/air | awk '{print \$3}' | awk 'NR == $i {print}'"
        strPermission="ls -l ~/modul2/air | awk 'NR == $i {print}'"
        printPerm=`eval $strPermission`
        printUser=`eval $strUser`
        printTitle=`eval $strTitle`
        echo ${printPerm:1:3}"_"$printUser"_"$printTitle >> ~/modul2/list.txt
    done
    # memindahkan list.txt ke folder air
    echo `mv ~/modul2/list.txt ~/modul2/air/.`
fi
